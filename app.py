from flask import request
import json
from flask import render_template
from db import db, User
from config import app


class DB:

    def get_users(self):
        users = db.session.query(User).all()
        list = []
        for u in users:
            list.append({"uid":u.uid, "login":u.login,"password":u.password, "name": u.name, "role": u.role})
        return list

    def get_user(self, login):
        u = User.query.filter_by(login=login).first()
        user = {"uid":u.uid, "login":u.login,"password":u.password, "name": u.name, "role": u.role}
        return user

    def add_user(self, login, password, name, role):
        user = User(login=login, password=password, name=name, role=role)
        db.session.add(user)
        db.session.commit()

    def delete_user(self, login):
        u = User.query.filter_by(login=login).first()
        db.session.delete(u)
        db.session.commit()

db_manager = DB()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/api/user/list')
def show_users():
    return json.dumps(db_manager.get_users())


@app.route('/api/user/profile/<login>')
def user_info(login):
    return json.dumps(db_manager.get_user(login))


@app.route('/api/user/delete/<login>')
def delete_user(login):
    db_manager.delete_user(login)
    return json.dumps(db_manager.get_users())


@app.route('/api/user/add', methods=['POST'])
def add_user():
    if request.method == 'POST':
        login = request.form['login']
        password = request.form['password']
        name = request.form['name']
        role = request.form['role']
        db_manager.add_user(login, password, name, role)
    return json.dumps(db_manager.get_users())


@app.route('/user/add')
def add_user_form():
    return render_template('add_user.html')

if __name__ == '__main__':
    app.run()
